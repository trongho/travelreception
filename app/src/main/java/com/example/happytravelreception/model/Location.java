package com.example.happytravelreception.model;

public class Location {
    private String provine;
    private String district;

    public Location(String provine, String district) {
        this.provine=provine;
        this.district = district;
    }

    public Location() {
    }

    public String getProvine() {
        return provine;
    }

    public void setProvine(String provine) {
        this.provine = provine;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }
}
