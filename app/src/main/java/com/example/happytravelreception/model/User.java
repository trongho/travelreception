package com.example.happytravelreception.model;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import com.example.happytravelreception.ultil.Role;

public class User extends BaseObservable {
    private String email;
    private String phone;
    private String fullname;
    private String avatarUrl;
    private String password;
    private Role role;
    private String hotelId;
    private String adminHotelId;

    public User(String email, String phone, String fullname, String avatarUrl, Role role) {
        this.email = email;
        this.phone = phone;
        this.fullname = fullname;
        this.avatarUrl = avatarUrl;
        this.role=role;
    }

    public User(String email, String phone, String fullname, String avatarUrl, Role role, String hotelId, String adminHotelId) {
        this.email = email;
        this.phone = phone;
        this.fullname = fullname;
        this.avatarUrl = avatarUrl;
        this.password = password;
        this.role = role;
        this.hotelId = hotelId;
        this.adminHotelId = adminHotelId;
    }

    public User() {
    }

    public String getHotelId() {
        return hotelId;
    }

    public void setHotelId(String hotelId) {
        this.hotelId = hotelId;
    }

    public String getAdminHotelId() {
        return adminHotelId;
    }

    public void setAdminHotelId(String adminHotelId) {
        this.adminHotelId = adminHotelId;
    }

    @Bindable
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
        notifyPropertyChanged(BR.email);
    }

    @Bindable
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
        notifyPropertyChanged(BR.phone);
    }

    @Bindable
    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
        notifyPropertyChanged(BR.fullname);
    }

    @Bindable
    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Bindable
    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    //    public boolean isEmailValid() {
//        return Patterns.EMAIL_ADDRESS.matcher(getEmail()).matches();
//    }
//
//    @Bindable
//    public String getPassword() {
//
//        if (password == null) {
//            return "";
//        }
//        return password;
//    }
//
//    public boolean isPasswordLengthGreaterThan5() {
//        return getPassword().length() > 5;
//    }

    public enum Gender {
        MALE, FEMALE
    }
}
