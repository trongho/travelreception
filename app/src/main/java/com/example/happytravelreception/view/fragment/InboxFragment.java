package com.example.happytravelreception.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.happytravelreception.Adapter.InboxAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentInboxBinding;
import com.example.happytravelreception.model.Inbox;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.view.MessageActivity;
import com.example.happytravelreception.viewmodel.InboxViewModel;

import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class InboxFragment extends Fragment {
    FragmentInboxBinding binding;
    InboxViewModel inboxViewModel;


    public InboxFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentInboxBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        inboxViewModel = ViewModelProviders.of(this).get(InboxViewModel.class);
        binding.setLifecycleOwner(this);

        initRecycleview();
    }

    public void initRecycleview() {
        inboxViewModel.getListInboxLivedata().observe(this, new Observer<List<Inbox>>() {
            @Override
            public void onChanged(List<Inbox> inboxes) {
                if(inboxes!=null) {
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    InboxAdapter adapter = new InboxAdapter(inboxes, new InboxAdapter.ItemClickListener() {
                        @Override
                        public void onClick(Inbox inbox) {
                            String receiverId = inbox.getReceiverId();
                            String senderId = inbox.getSenderId();
                            Common.HOTEL_NAME = inbox.getReceiverName();
                            Common.HOTEL_ID = receiverId;
                            Common.CUSTOMER_ID = senderId;
                            Common.CUSTOMER_NAME = inbox.getSenderName();
                            Common.IMAGE_URL = inbox.getProfilePictureSender();
                            Common.HOTEL_IMAGE = inbox.getProfilePictureReceiver();
                            Intent intent = new Intent(getContext(), MessageActivity.class);
                            startActivity(intent);
                        }
                    });
                    binding.recyclerview.setLayoutManager(layoutManager);
                    binding.recyclerview.setAdapter(adapter);
                }
            }
        });
    }
}
