package com.example.happytravelreception.view.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.happytravelreception.Adapter.HotelBookingAdapter;
import com.example.happytravelreception.Adapter.HotelBookingAdapter2;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentCheckinBinding;
import com.example.happytravelreception.databinding.FragmentWaitingCheckinBinding;
import com.example.happytravelreception.model.HotelBooking;
import com.example.happytravelreception.ultil.Status;
import com.example.happytravelreception.viewmodel.HotelBookingViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class CheckinFragment extends Fragment {
    FragmentCheckinBinding binding;
    HotelBookingViewModel hotelBookingViewModel;
    UserViewmodel userViewmodel;
    String userId;
    String hotelId;


    public CheckinFragment() {
        // Required empty public constructor
    }

    public static CheckinFragment newInstance() {
        CheckinFragment fragment = new CheckinFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentCheckinBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hotelBookingViewModel= ViewModelProviders.of(this).get(HotelBookingViewModel.class);
        userViewmodel=ViewModelProviders.of(this).get(UserViewmodel.class);
        binding.setLifecycleOwner(this);

        initRecycleview();

        hotelBookingViewModel.getBookingUpdateIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                Toast.makeText(getContext(), "Đã checkin", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initRecycleview() {
        LiveData<List<HotelBooking>> listLiveData = hotelBookingViewModel.getHotelbookingListLivedata2();
        listLiveData.observe(this, new Observer<List<HotelBooking>>() {
            @Override
            public void onChanged(List<HotelBooking> hotelBookings) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        HotelBookingAdapter2 adapter = new HotelBookingAdapter2(hotelBookings, new HotelBookingAdapter2.ItemClickListener() {
                            @Override
                            public void onClick(HotelBooking hotelBooking) {
                                //item click
                            }
                        }, new HotelBookingAdapter2.ItemLongClickListener() {
                            @Override
                            public boolean onLongClick(HotelBooking hotelBooking) {
                                //item long ckick
                                return false;
                            }
                        }, new HotelBookingAdapter2.CheckinClickListener() {
                            @Override
                            public void onClick(HotelBooking hotelBooking) {
                                hotelBookingViewModel.updateHotelBooking(hotelBooking.getBookingId());
                            }
                        });
                        binding.recyclerview.setLayoutManager(layoutManager);
                        binding.recyclerview.setAdapter(adapter);
            }
        });
    }
}
