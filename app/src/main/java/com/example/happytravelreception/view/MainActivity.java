package com.example.happytravelreception.view;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import android.widget.Toolbar;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityMainBinding;
import com.example.happytravelreception.ultil.BottomNavigationBehavior;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.example.happytravelreception.view.fragment.AccountFragment;
import com.example.happytravelreception.view.fragment.BookingManagerFragment;
import com.example.happytravelreception.view.fragment.HomeFragment;
import com.example.happytravelreception.view.fragment.InboxFragment;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {
    ActivityMainBinding binding;
    UserViewmodel userViewmodel;
    String userUID;
    Role userRole;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        userViewmodel = ViewModelProviders.of(this).get(UserViewmodel.class);
        binding.setLifecycleOwner(this);
        //toolbar
        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.toolbar_main);


        userRole = Common.ROLE;
        if (userRole != null && userRole==Role.ADMIN_HOTEL) {
            loadFragment(new HomeFragment());
        }else
            loadFragment(new BookingManagerFragment());


        binding.navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()) {
                    case R.id.navigation_main:
                        if (userRole != null && userRole==Role.ADMIN_HOTEL) {
                            getSupportActionBar().setTitle("Quản lý khách sạn");
                            fragment = new HomeFragment();
                            loadFragment(fragment);
                            return true;
                        } else {
                            getSupportActionBar().setTitle("Quản lý booking");
                            fragment = new BookingManagerFragment();
                            loadFragment(fragment);
                            return true;
                        }
                    case R.id.navigation_inbox:
                            getSupportActionBar().setTitle("Inbox");
                            fragment = new InboxFragment();
                            loadFragment(fragment);
                            return true;
                    case R.id.navigation_account:
                        getSupportActionBar().setTitle("Account");
                        fragment = new AccountFragment();
                        loadFragment(fragment);
                        return true;
                }
                return false;
            }
        });

        // attaching bottom sheet behaviour - hide / show on scroll
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) binding.navigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());
    }

    @Override
    protected void onStart() {
        super.onStart();
        userViewmodel.getUserLiveData().observe(MainActivity.this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                userUID = firebaseUser.getUid();
                Common.USER_ID=userUID;
                userViewmodel.getHotelId(userUID);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        binding = null;
    }


    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}
