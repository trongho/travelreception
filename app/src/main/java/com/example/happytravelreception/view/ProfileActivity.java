package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityProfileBinding;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

public class ProfileActivity extends AppCompatActivity {
    ActivityProfileBinding binding;
    UserViewmodel userViewmodel;
    private static final int RC_PHOTO_PICKER = 1;
    String userUID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_profile);
        userViewmodel = ViewModelProviders.of(this).get(UserViewmodel.class);
        binding.setLifecycleOwner(this);
        binding.setUserviewmodel(userViewmodel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.account_info);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        userViewmodel.getUserUpdateIsSuccessful().observe(this, isSuccessful -> {
            if (isSuccessful) {
                Toast.makeText(getApplicationContext(), "Update done", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getApplicationContext(), "login fail", Toast.LENGTH_LONG).show();
            }
        });

        binding.profileImage.setOnClickListener(v->{
            chooseImage();
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    public void chooseImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Action canceled", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (requestCode) {
            case RC_PHOTO_PICKER:
                Uri imgUri = data.getData();
                Picasso.get().load(imgUri).placeholder(R.drawable.ic_image_black_24dp).into(binding.profileImage);
                userViewmodel.uploadImage(data);
                break;
            default:
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    userUID=firebaseUser.getUid();
                    userViewmodel.getUserDetail(userUID);
                } else {

                }
            }
        });
    }

}
