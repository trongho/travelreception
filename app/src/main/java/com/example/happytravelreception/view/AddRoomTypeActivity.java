package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityAddRoomTypeBinding;
import com.example.happytravelreception.viewmodel.AddHotelViewModel;
import com.example.happytravelreception.viewmodel.AddRoomTypeViewmodel;
import com.example.happytravelreception.viewmodel.HotelViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.squareup.picasso.Picasso;

public class AddRoomTypeActivity extends AppCompatActivity {
    ActivityAddRoomTypeBinding binding;
    AddRoomTypeViewmodel addRoomTypeViewmodel;
    private static final int RC_PHOTO_PICKER = 1;
    String hotel_uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_room_type);
        addRoomTypeViewmodel = ViewModelProviders.of(this).get(AddRoomTypeViewmodel.class);
        binding.setLifecycleOwner(this);
        binding.setAddroomtypeviewmodel(addRoomTypeViewmodel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.title_addroomtype);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.imageUrl.setOnClickListener(v->{
            chooseImage();
        });


        binding.add.setOnClickListener(v->{
            getHotelIdIntent();
            addRoomTypeViewmodel.addRoomType(hotel_uid);
        });

        addRoomTypeViewmodel.getRoomtypeCreatIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void chooseImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Action canceled", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (requestCode) {
            case RC_PHOTO_PICKER:
                Uri imgUri = data.getData();
                Picasso.get().load(imgUri).placeholder(R.drawable.ic_image_black_24dp).into(binding.imageUrl);
                addRoomTypeViewmodel.uploadImage(data);
                break;
            default:
        }

    }

    public void getHotelIdIntent(){
        Intent intent=getIntent();
        hotel_uid=intent.getStringExtra("hotelid");
    }

}
