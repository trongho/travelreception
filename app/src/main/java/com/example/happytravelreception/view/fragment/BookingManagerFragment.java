package com.example.happytravelreception.view.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentAccountBinding;
import com.example.happytravelreception.databinding.FragmentBookingManagerBinding;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingManagerFragment extends Fragment {
    FragmentBookingManagerBinding binding;

    public BookingManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentBookingManagerBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.setHandler(this);
        binding.setManager(getFragmentManager());
    }


}
