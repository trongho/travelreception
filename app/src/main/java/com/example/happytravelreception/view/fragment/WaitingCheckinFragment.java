package com.example.happytravelreception.view.fragment;


import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.happytravelreception.Adapter.HotelBookingAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentHomeBinding;
import com.example.happytravelreception.databinding.FragmentWaitingCheckinBinding;
import com.example.happytravelreception.model.HotelBooking;
import com.example.happytravelreception.model.User;
import com.example.happytravelreception.ultil.Status;
import com.example.happytravelreception.viewmodel.HotelBookingViewModel;
import com.example.happytravelreception.viewmodel.HotelViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class WaitingCheckinFragment extends Fragment {
    FragmentWaitingCheckinBinding binding;
    HotelBookingViewModel hotelBookingViewModel;
    UserViewmodel userViewmodel;
    HotelViewModel hotelViewModel;
    String userId;
    String hotelId;


    public WaitingCheckinFragment() {
        // Required empty public constructor
    }

    public static WaitingCheckinFragment newInstance() {
        WaitingCheckinFragment fragment = new WaitingCheckinFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentWaitingCheckinBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        hotelBookingViewModel= ViewModelProviders.of(this).get(HotelBookingViewModel.class);
        userViewmodel=ViewModelProviders.of(this).get(UserViewmodel.class);
        hotelViewModel=ViewModelProviders.of(this).get(HotelViewModel.class);
        binding.setLifecycleOwner(this);


        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                userId=firebaseUser.getUid();
                hotelId=userViewmodel.getHotelId(userId);

            }
        });
        initRecycleview();

        hotelBookingViewModel.getBookingUpdateIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                Toast.makeText(getContext(), "Đã checkin", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });

        hotelBookingViewModel.getRemoveBookingIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                Toast.makeText(getContext(), "Xóa booking thành công", Toast.LENGTH_LONG).show();
            } else {
                Toast.makeText(getContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initRecycleview() {
        LiveData<List<HotelBooking>> listLiveData = hotelBookingViewModel.getHotelbookingListLivedata();
        listLiveData.observe(this, new Observer<List<HotelBooking>>() {
            @Override
            public void onChanged(List<HotelBooking> hotelBookings) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        HotelBookingAdapter adapter = new HotelBookingAdapter(hotelBookings, new HotelBookingAdapter.ItemClickListener() {
                            @Override
                            public void onClick(HotelBooking hotelBooking) {
                                //item click
                            }
                        }, new HotelBookingAdapter.ItemLongClickListener() {
                            @Override
                            public boolean onLongClick(HotelBooking hotelBooking) {
                                //item long ckick
                                return false;
                            }
                        }, new HotelBookingAdapter.CheckinClickListener() {
                            @Override
                            public void onClick(HotelBooking hotelBooking) {
                                hotelBookingViewModel.updateHotelBooking(hotelBooking.getBookingId());
                            }
                        }, new HotelBookingAdapter.RemoveClickListener() {
                            @Override
                            public void onClick(HotelBooking hotelBooking) {
                                hotelBookingViewModel.removeBooking(hotelBooking.getBookingId());
                            }
                        });
                        binding.recyclerview.setLayoutManager(layoutManager);
                        binding.recyclerview.setAdapter(adapter);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();

    }
}
