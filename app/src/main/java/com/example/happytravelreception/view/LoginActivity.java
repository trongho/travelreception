package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityLoginBinding;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.example.happytravelreception.viewmodel.LoginViewModel;
import com.google.firebase.auth.FirebaseUser;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";
    ActivityLoginBinding binding;
    LoginViewModel loginViewModel;
    static String pw;
    static String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        loginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setLoginviewmodel(loginViewModel);

        binding.role.setOnClickListener(v->{
            loginRole();
        });

        loginViewModel.getUserLoginIsSuccessful().observe(this, isSuccessful -> {
            if (isSuccessful) {
                startActivity(new Intent(this, MainActivity.class));
                pw=binding.password.getEditText().getText().toString();
                email=binding.email.getEditText().getText().toString();
            } else {
                Toast.makeText(getApplicationContext(), "login fail", Toast.LENGTH_LONG).show();
            }
        });

        binding.switchregister.setOnClickListener(v -> {
            startActivity(new Intent(this, RegisterActivity.class));
        });

    }

    public void loginRole(){
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setTitle("Chọn tài khoản");
        String[] roles = {"Admin hotel", "Reception"};
        builder.setItems(roles, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                switch (which) {
                    case 0: binding.role.setText("ADMIN_HOTEL");
                        Common.ROLE = Role.ADMIN_HOTEL;break;
                    case 1:binding.role.setText("RECEPTION");Common.ROLE = Role.RECEPTION;break;
                }
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    @Override
    protected void onStart() {
        super.onStart();
        loginViewModel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if(firebaseUser!=null){
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                }
            }
        });
    }
}
