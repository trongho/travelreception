package com.example.happytravelreception.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentAccountBinding;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.example.happytravelreception.view.LoginActivity;
import com.example.happytravelreception.view.MainActivity;
import com.example.happytravelreception.view.ProfileActivity;
import com.example.happytravelreception.view.ReceptionistManagerActivity;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    FragmentAccountBinding binding;
    UserViewmodel userViewmodel;
    String userID;
    Role userRole;


    public AccountFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentAccountBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userViewmodel = ViewModelProviders.of(this).get(UserViewmodel.class);


        binding.profile.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), ProfileActivity.class));
        });

        binding.staffManager.setOnClickListener(v -> {
            startActivity(new Intent(getContext(), ReceptionistManagerActivity.class));
        });

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.account_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            userViewmodel.logout();
            startActivity(new Intent(getContext(), LoginActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onStart() {
        super.onStart();
        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    ((MainActivity) getContext()).getSupportActionBar().setTitle("Xin chào " + firebaseUser.getEmail());
                    userID=firebaseUser.getUid();
                    if(Common.ROLE!=null) {
                        userRole = Common.ROLE;
                        if (userRole!=Role.ADMIN_HOTEL) {
                            binding.staffManager.setClickable(false);
                        }
                    }
                } else {
                }
            }
        });
    }
}
