package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import com.example.happytravelreception.Adapter.HotelAdapter;
import com.example.happytravelreception.Adapter.RoomTypeAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityHotelDetailBinding;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.model.RoomType;
import com.example.happytravelreception.ultil.TypeHotel;
import com.example.happytravelreception.viewmodel.AddHotelViewModel;
import com.example.happytravelreception.viewmodel.HotelViewModel;
import com.example.happytravelreception.viewmodel.RoomTypeViewmodel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class HotelDetailActivity extends AppCompatActivity {
    ActivityHotelDetailBinding binding;
    HotelViewModel hotelViewModel;
    RoomTypeViewmodel roomTypeViewmodel;
    private static final int RC_PHOTO_PICKER = 1;
    String hotel_uid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_hotel_detail);
        hotelViewModel = ViewModelProviders.of(this).get(HotelViewModel.class);
        roomTypeViewmodel=ViewModelProviders.of(this).get(RoomTypeViewmodel.class);
        binding.setLifecycleOwner(this);
        binding.setHotelviewmodel(hotelViewModel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.hotel_detail);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        getHotelIdIntent();
        hotelViewModel.getHotelDetail(hotel_uid);

        binding.update.setOnClickListener(v->{
            hotelViewModel.updateHotel(hotel_uid);
        });
        hotelViewModel.getHotelUpdateIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                Toast.makeText(getApplicationContext(),"Update done",Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });

        binding.imageUrl.setOnClickListener(v->{
            chooseImage();
        });

        binding.addroomtype.setOnClickListener(v->{
            Intent intent=new Intent(HotelDetailActivity.this,AddRoomTypeActivity.class);
            intent.putExtra("hotelid",hotel_uid);
            startActivity(intent);
        });

        initRecycleview();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding = null;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    public void getHotelIdIntent(){
        Intent intent=getIntent();
        hotel_uid=intent.getStringExtra("hotel_uid");

    }

    public void chooseImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Action canceled", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (requestCode) {
            case RC_PHOTO_PICKER:
                Uri imgUri = data.getData();
                Picasso.get().load(imgUri).placeholder(R.drawable.ic_image_black_24dp).into(binding.imageUrl);
                hotelViewModel.uploadImage(data);
                break;
            default:
        }

    }

    private void initRecycleview(){
        LiveData<List<RoomType>> listLiveData=roomTypeViewmodel.getRoomtypeListLivedata();
        listLiveData.observe(this, new Observer<List<RoomType>>() {
            @Override
            public void onChanged(List<RoomType> roomTypes) {
                List<RoomType> newList=new ArrayList<>();
                for(int i=0;i<roomTypes.size();i++){
                    if(roomTypes.get(i).getHotelId().equalsIgnoreCase(hotel_uid)){
                        newList.add(roomTypes.get(i));
                    }
                }

                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                RoomTypeAdapter adapter=new RoomTypeAdapter(newList, new RoomTypeAdapter.ItemClickListener() {
                    @Override
                    public void onClick(RoomType roomType) {
                        //item click
                    }
                }, new RoomTypeAdapter.ItemLongClickListener() {
                    @Override
                    public boolean onLongClick(RoomType roomType) {
                        //item long ckick
                        return false;
                    }
                });
                binding.recyclerview.setLayoutManager(layoutManager);
                binding.recyclerview.setAdapter(adapter);
            }
        });
    }


}
