package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.happytravelreception.Adapter.UserAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityReceptionistManagerBinding;
import com.example.happytravelreception.model.User;
import com.example.happytravelreception.viewmodel.RegisterViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class ReceptionistManagerActivity extends AppCompatActivity {
    ActivityReceptionistManagerBinding binding;
    UserViewmodel userViewmodel;
    RegisterViewModel registerViewModel;
    String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_receptionist_manager);
        userViewmodel= ViewModelProviders.of(this).get(UserViewmodel.class);
        registerViewModel=ViewModelProviders.of(this).get(RegisterViewModel.class);
        binding.setLifecycleOwner(this);

        binding.add.setOnClickListener(v->{
                startActivity(new Intent(this,AddReceptionActivity.class));
        });

        initRecycleview();

    }

    private void initRecycleview(){
        LiveData<List<User>> listLiveData=userViewmodel.getUserListLivedata();
        listLiveData.observe(this, new Observer<List<User>>() {
            @Override
            public void onChanged(List<User> users) {
                List<User> newList=new ArrayList<>();
                for(int i=0;i<users.size();i++) {
                    if (users.get(i).getAdminHotelId()!=null&&users.get(i).getAdminHotelId().equalsIgnoreCase(userId)) {
                        newList.add(users.get(i));
                    }
                }
                LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                UserAdapter adapter=new UserAdapter(newList, new UserAdapter.ItemClickListener() {
                    @Override
                    public void onClick(User user) {
                        //item click
                    }
                }, new UserAdapter.ItemLongClickListener() {
                    @Override
                    public boolean onLongClick(User user) {
                        //item long ckick
                        return false;
                    }
                });
                binding.recyclerview.setLayoutManager(layoutManager);
                binding.recyclerview.setAdapter(adapter);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    userId=firebaseUser.getUid();
                } else {

                }
            }
        });
    }
}
