package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityAddHotelBinding;
import com.example.happytravelreception.model.Location;
import com.example.happytravelreception.ultil.PopUpClass;
import com.example.happytravelreception.ultil.StandardStar;
import com.example.happytravelreception.ultil.TypeHotel;
import com.example.happytravelreception.viewmodel.AddHotelViewModel;
import com.example.happytravelreception.viewmodel.LocationViewmodel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AddHotelActivity extends AppCompatActivity {
    ActivityAddHotelBinding binding;
    AddHotelViewModel addHotelViewModel;
    LocationViewmodel locationViewmodel;
        private static final int RC_PHOTO_PICKER = 1;
    String userId;
    String selectedprovine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_add_hotel);
        addHotelViewModel = ViewModelProviders.of(this).get(AddHotelViewModel.class);
        locationViewmodel=ViewModelProviders.of(this).get(LocationViewmodel.class);
        binding.setLifecycleOwner(this);
        binding.setAddhotelviewmodel(addHotelViewModel);

        setSupportActionBar(binding.toolbar);
        getSupportActionBar().setTitle(R.string.title_addhotel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        binding.profileImage.setOnClickListener(v -> {
            chooseImage();
        });

        addHotelViewModel.getImageUploadIsSuccessful().observe(this, isSuccessful -> {
            if (isSuccessful) {
            } else {
                Toast.makeText(getApplicationContext(), "Could not fetch the picture!", Toast.LENGTH_LONG).show();
            }
        });


        addHotelViewModel.getHotelCreatIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                finish();
            } else {
                Toast.makeText(getApplicationContext(), "Không thành công", Toast.LENGTH_LONG).show();
            }
        });

        binding.typeHotel.setOnClickListener(v->{
                chooseTypeHotel();

        });
        binding.standardStar.setOnClickListener(v->{
                chooseStandardStar();
        });

        binding.provine.setOnClickListener(v->{
            chooseProvine();
        });

        binding.district.setOnClickListener(v->{
            chooseDistrict();
        });

        binding.openhours.setOnClickListener(v->{
            setOpenhours();
        });

        binding.addHotel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getUserIdIntent();
                addHotelViewModel.addHotel(userId);
            }
        });
    }

    public void setOpenhours() {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);

        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(AddHotelActivity.this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                binding.openhours.setText( selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void chooseProvine(){
        List<String> listProvine=new ArrayList<>();
        locationViewmodel.getLocationLiveData().observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(List<Location> locations) {
                for(int i=0;i<locations.size();i++){
                    listProvine.add(locations.get(i).getProvine());
                }
                HashSet<String> hashSet = new HashSet<String>();
                hashSet.addAll(listProvine);
                listProvine.clear();
                listProvine.addAll(hashSet);

                PopUpClass popUpClass = new PopUpClass(new PopUpClass.PopupItemSelected() {
                    @Override
                    public void onClick(String result) {
                        binding.provine.setText(result);
                        selectedprovine=binding.provine.getText().toString();
                    }
                });
                popUpClass.showPopupWindow(binding.provine,listProvine, getApplicationContext());

            }
        });

    }

    public void chooseDistrict(){
        List<String> listDistrict=new ArrayList<>();
        locationViewmodel.getLocationLiveData().observe(this, new Observer<List<Location>>() {
            @Override
            public void onChanged(List<Location> locations) {
                for(int i=0;i<locations.size();i++){
                    if(locations.get(i).getProvine().equalsIgnoreCase(selectedprovine)) {
                        listDistrict.add(locations.get(i).getDistrict());
                    }
                }
                PopUpClass popUpClass = new PopUpClass(new PopUpClass.PopupItemSelected() {
                    @Override
                    public void onClick(String result) {
                        binding.district.setText(result);
                    }
                });
                popUpClass.showPopupWindow(binding.district,listDistrict, getApplicationContext());
            }
        });

    }

    public void chooseTypeHotel(){
        List<String> list = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            list = Stream.of(TypeHotel.values())
                    .map(TypeHotel::name)
                    .collect(Collectors.toList());
        }
        PopUpClass popUpClass = new PopUpClass(new PopUpClass.PopupItemSelected() {
            @Override
            public void onClick(String result) {
                binding.typeHotel.setText(result);
            }
        });
        popUpClass.showPopupWindow(binding.typeHotel, list, getApplicationContext());
    }

    public void chooseStandardStar(){
        List<String> list = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            list = Stream.of(StandardStar.values())
                    .map(StandardStar::name)
                    .collect(Collectors.toList());
        }
        PopUpClass popUpClass = new PopUpClass(new PopUpClass.PopupItemSelected() {
            @Override
            public void onClick(String result) {
                binding.standardStar.setText(result);
            }
        });
        popUpClass.showPopupWindow(binding.standardStar, list, getApplicationContext());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        finish();
        return true;
    }

    public void chooseImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Action canceled", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (requestCode) {
            case RC_PHOTO_PICKER:
                Uri imgUri = data.getData();
                Picasso.get().load(imgUri).placeholder(R.drawable.ic_image_black_24dp).into(binding.profileImage);
                addHotelViewModel.uploadImage(data);
                break;
            default:
        }

    }

    public void getUserIdIntent(){
        Intent intent=getIntent();
        userId=intent.getStringExtra("userId");
    }

    public interface OrderItemViewModelListener {
        void onCheckChanged();
    }
}
