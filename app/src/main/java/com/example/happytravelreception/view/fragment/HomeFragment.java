package com.example.happytravelreception.view.fragment;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.happytravelreception.Adapter.HotelAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.FragmentAccountBinding;
import com.example.happytravelreception.databinding.FragmentHomeBinding;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.view.AddHotelActivity;
import com.example.happytravelreception.view.HotelDetailActivity;
import com.example.happytravelreception.view.LoginActivity;
import com.example.happytravelreception.view.MainActivity;
import com.example.happytravelreception.view.ProfileActivity;
import com.example.happytravelreception.viewmodel.HotelViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    FragmentHomeBinding binding;
    UserViewmodel userViewmodel;
    HotelViewModel hotelViewModel;
    String userId;


    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = FragmentHomeBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        userViewmodel= ViewModelProviders.of(this).get(UserViewmodel.class);
        hotelViewModel=ViewModelProviders.of(this).get(HotelViewModel.class);

        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    ((MainActivity)getContext()).getSupportActionBar().setTitle("Xin chào "+firebaseUser.getEmail());

                    userId=firebaseUser.getUid();
                } else {

                }
            }
        });

        initRecycleview();

    }

    private void initRecycleview(){
        LiveData<List<Hotel>> listLiveData=hotelViewModel.getHotelListLivedata();
        listLiveData.observe(this, new Observer<List<Hotel>>() {
            @Override
            public void onChanged(List<Hotel> hotels) {
                List<Hotel> newList=new ArrayList<>();
                for(int i=0;i<hotels.size();i++) {
                    if (hotels.get(i).getUserId().equalsIgnoreCase(userId)) {
                        newList.add(hotels.get(i));
                    }
                }
                LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                HotelAdapter adapter=new HotelAdapter(newList, new HotelAdapter.ItemClickListener() {
                    @Override
                    public void onClick(Hotel hotel) {
                        //item click
                        String hotelId=hotel.getHotelId();
                        Intent intent=new Intent(getContext(),HotelDetailActivity.class);
                        intent.putExtra("hotel_uid",hotelId);
                        startActivity(intent);
                    }
                }, new HotelAdapter.ItemLongClickListener() {
                    @Override
                    public boolean onLongClick(Hotel hotel) {
                        //item long ckick
                        return false;
                    }
                });

                binding.recyclerview.setLayoutManager(layoutManager);
                binding.recyclerview.setAdapter(adapter);
            }
        });
    }




    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu,menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_addhotel) {
            userViewmodel.logout();
            Intent intent=new Intent(getContext(),AddHotelActivity.class);
            intent.putExtra("userId",userId);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
