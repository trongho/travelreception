package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityAddReceptionBinding;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.model.Location;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.PopUpClass;
import com.example.happytravelreception.viewmodel.AddReceptionViewModel;
import com.example.happytravelreception.viewmodel.HotelViewModel;
import com.example.happytravelreception.viewmodel.LoginViewModel;
import com.example.happytravelreception.viewmodel.RegisterViewModel;
import com.example.happytravelreception.viewmodel.UserViewmodel;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

public class AddReceptionActivity extends AppCompatActivity {
    ActivityAddReceptionBinding binding;
    UserViewmodel userViewmodel;
    AddReceptionViewModel addReceptionViewModel;
    LoginViewModel loginViewModel;
    HotelViewModel hotelViewModel;
    String hotelId;
    String userId,emailAdmin,passwordAdmin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_add_reception);
        userViewmodel= ViewModelProviders.of(this).get(UserViewmodel.class);
        addReceptionViewModel=ViewModelProviders.of(this).get(AddReceptionViewModel.class);
        loginViewModel=ViewModelProviders.of(this).get(LoginViewModel.class);
        hotelViewModel=ViewModelProviders.of(this).get(HotelViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setRegisterViewmodel(addReceptionViewModel);


        binding.hotelId.setOnClickListener(v->{
            chooseHotelId();
        });

        binding.register.setOnClickListener(v->{
            addReceptionViewModel.addReceptionist(hotelId,userId);
        });

        addReceptionViewModel.getAddReceptionIsSuccessful().observe(this,isSuccessful->{
            if (isSuccessful) {
                finish();
                loginViewModel.login(emailAdmin,passwordAdmin);
            } else {
                Toast.makeText(getApplicationContext(), "Thêm thất bại", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void chooseHotelId(){
        List<String> newList=new ArrayList<>();
        hotelViewModel.getHotelListLivedata().observe(this, new Observer<List<Hotel>>() {
            @Override
            public void onChanged(List<Hotel> hotels) {
                for(int i=0;i<hotels.size();i++) {
                    if (hotels.get(i).getUserId().equalsIgnoreCase(Common.USER_ID)) {
                        newList.add(hotels.get(i).getHotelId());
                    }
                }
                PopUpClass popUpClass = new PopUpClass(new PopUpClass.PopupItemSelected() {
                    @Override
                    public void onClick(String result) {
                        binding.hotelId.setText(result);
                        hotelId=result;
                    }
                });
                popUpClass.showPopupWindow(binding.hotelId,newList, getApplicationContext());
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        userViewmodel.getUserLiveData().observe(this, new Observer<FirebaseUser>() {
            @Override
            public void onChanged(FirebaseUser firebaseUser) {
                if (firebaseUser != null) {
                    userId=firebaseUser.getUid();
                    emailAdmin=LoginActivity.email;
                    passwordAdmin=LoginActivity.pw;
                } else {

                }
            }
        });
    }
}
