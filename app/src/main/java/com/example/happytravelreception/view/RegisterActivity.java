package com.example.happytravelreception.view;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProviders;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ActivityRegisterBinding;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.example.happytravelreception.viewmodel.RegisterViewModel;
import com.squareup.picasso.Picasso;

public class RegisterActivity extends AppCompatActivity {
    ActivityRegisterBinding binding;
    RegisterViewModel registerViewModel;
    private static final int RC_PHOTO_PICKER = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        registerViewModel = ViewModelProviders.of(this).get(RegisterViewModel.class);
        binding.setLifecycleOwner(this);
        binding.setRegisterViewmodel(registerViewModel);

        registerViewModel.getUserCreatIsSuccessful().observe(this, isSuccessful -> {
            if (isSuccessful) {
                startActivity(new Intent(this, MainActivity.class));
                Common.ROLE= Role.ADMIN_HOTEL;
            } else {
                Toast.makeText(getApplicationContext(), "login fail", Toast.LENGTH_LONG).show();
            }
        });

        binding.imageUrl.setOnClickListener(v->{
            chooseImage();
        });
    }

    public void chooseImage() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
        startActivityForResult(Intent.createChooser(intent, "complete action using"), RC_PHOTO_PICKER);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            Toast.makeText(getApplicationContext(), "Action canceled", Toast.LENGTH_SHORT).show();
            return;
        }

        switch (requestCode) {
            case RC_PHOTO_PICKER:
                Uri imgUri = data.getData();
                Picasso.get().load(imgUri).placeholder(R.drawable.ic_image_black_24dp).into(binding.imageUrl);
                registerViewModel.uploadImage(data);
                break;
            default:
        }

    }
}
