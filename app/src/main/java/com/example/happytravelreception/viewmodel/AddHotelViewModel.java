package com.example.happytravelreception.viewmodel;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.ultil.PaymentMethods;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class AddHotelViewModel extends ViewModel {
    private static final DatabaseReference HOTEL_REF = FirebaseDatabase.getInstance().getReference("/hotels");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOTEL_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private final MutableLiveData<Boolean> hotelCreatIsSuccessful = new MutableLiveData<>();
    private final MutableLiveData<Boolean> imageUploadIsSuccessful = new MutableLiveData<>();

    public MutableLiveData<Boolean> getHotelCreatIsSuccessful() {
        return hotelCreatIsSuccessful;
    }

    public MutableLiveData<Boolean> getImageUploadIsSuccessful() {
        return imageUploadIsSuccessful;
    }


    public MutableLiveData<String> hotelId = new MutableLiveData<>();
    public MutableLiveData<String> hotelName = new MutableLiveData<>();
    public MutableLiveData<String> imageUrl = new MutableLiveData<>();
    public MutableLiveData<String> provine = new MutableLiveData<>();
    public MutableLiveData<String> district = new MutableLiveData<>();
    public MutableLiveData<String> description = new MutableLiveData<>();
    public MutableLiveData<String> adress = new MutableLiveData<>();
    public MutableLiveData<String> standarStar = new MutableLiveData<>();
    public MutableLiveData<String> typeHotel = new MutableLiveData<>();
    public MutableLiveData<PaymentMethods> paymentMethods = new MutableLiveData<>();
    public MutableLiveData<String> openhours = new MutableLiveData<>();

    public static final String GYM = "Gym";
    public static final String WIFI = "Wifi";

    Map<String, Boolean> utilities = new HashMap<>();

    public Map<String, Boolean> getUtilities() {
        utilities.put(GYM, false);
        utilities.put(WIFI, false);
        return utilities;
    }


    public void addHotel(String userId) {
        String key=HOTEL_REF.push().getKey();
        hotelId.setValue(key);
        Task uploadTask = HOTEL_REF
                .child(key)
                .setValue(new Hotel(hotelId.getValue(),hotelName.getValue(), imageUrl.getValue(), provine.getValue(),district.getValue(),
                        description.getValue(), adress.getValue(), standarStar.getValue(), typeHotel.getValue(), utilities,
                        PaymentMethods.CASH, openhours.getValue(),userId));
        uploadTask.addOnSuccessListener(o -> hotelCreatIsSuccessful.setValue(true));
    }

    public String uploadImage(Intent intentData) {
        Uri selectedUri = intentData.getData();
        StorageReference photoRef = FirebaseStorage.getInstance()
                .getReference().child("hotels")
                .child(selectedUri.getLastPathSegment());

        UploadTask uploadTask = photoRef.putFile(selectedUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    imageUrl.setValue(String.valueOf(downloadUri));
                    imageUploadIsSuccessful.setValue(true);
                } else {
                    imageUploadIsSuccessful.setValue(false);
                }
            }
        });
        return imageUrl.getValue();
    }
}
