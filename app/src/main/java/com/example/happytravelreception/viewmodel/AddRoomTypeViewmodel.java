package com.example.happytravelreception.viewmodel;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.model.RoomType;
import com.example.happytravelreception.ultil.PaymentMethods;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.HashMap;
import java.util.Map;

public class AddRoomTypeViewmodel extends ViewModel {
    private static final DatabaseReference ROOMTYPE_REF = FirebaseDatabase.getInstance().getReference("/roomtypes");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(ROOMTYPE_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private final MutableLiveData<Boolean> roomtypeCreatIsSuccessful = new MutableLiveData<>();
    private final MutableLiveData<Boolean> imageUploadIsSuccessful = new MutableLiveData<>();

    public MutableLiveData<Boolean> getRoomtypeCreatIsSuccessful() {
        return roomtypeCreatIsSuccessful;
    }

    public MutableLiveData<Boolean> getImageUploadIsSuccessful() {
        return imageUploadIsSuccessful;
    }

    public MutableLiveData<String> roomTypeId = new MutableLiveData<>();
    public MutableLiveData<String> typeName = new MutableLiveData<>();
    public MutableLiveData<String> imageUrl = new MutableLiveData<>();
    public MutableLiveData<Integer> capacity = new MutableLiveData<>();
    public MutableLiveData<String> description = new MutableLiveData<>();
    public MutableLiveData<Integer> quantity = new MutableLiveData<>();
    public MutableLiveData<Float> price = new MutableLiveData<>();
    public MutableLiveData<String> hotelId = new MutableLiveData<>();

    public void addRoomType(String hotelId) {
        String key=ROOMTYPE_REF.push().getKey();
        roomTypeId.setValue(key);
        Task uploadTask = ROOMTYPE_REF
                .child(key)
                .setValue(new RoomType(roomTypeId.getValue(),typeName.getValue(),imageUrl.getValue(),capacity.getValue(),description.getValue(),
                        quantity.getValue(),price.getValue(),hotelId));
        uploadTask.addOnSuccessListener(o -> roomtypeCreatIsSuccessful.setValue(true));
    }

    public String uploadImage(Intent intentData) {
        Uri selectedUri = intentData.getData();
        StorageReference photoRef = FirebaseStorage.getInstance()
                .getReference().child("roomtypes")
                .child(selectedUri.getLastPathSegment());

        UploadTask uploadTask = photoRef.putFile(selectedUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    imageUrl.setValue(String.valueOf(downloadUri));
                    imageUploadIsSuccessful.setValue(true);
                } else {
                    imageUploadIsSuccessful.setValue(false);
                }
            }
        });
        return imageUrl.getValue();
    }
}
