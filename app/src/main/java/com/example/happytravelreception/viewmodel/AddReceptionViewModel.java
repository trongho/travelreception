package com.example.happytravelreception.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.User;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AddReceptionViewModel extends ViewModel {
    private static final DatabaseReference USER_REF = FirebaseDatabase.getInstance().getReference("/users");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(USER_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    public MutableLiveData<String> email=new MutableLiveData<>();
    public MutableLiveData<String> imageUrl=new MutableLiveData<>();
    public MutableLiveData<String> fullname=new MutableLiveData<>();
    public MutableLiveData<String> phone=new MutableLiveData<>();
    public MutableLiveData<String> password=new MutableLiveData<>();
    public MutableLiveData<String> confpassword=new MutableLiveData<>();

    private final MutableLiveData<Boolean> addReceptionIsSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getAddReceptionIsSuccessful() {
        return addReceptionIsSuccessful;
    }
    public void addReceptionist(String hotelId,String adminHotelId) {
        mAuth.createUserWithEmailAndPassword(email.getValue(), password.getValue()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                String uid = FirebaseAuth.getInstance().getUid();
                Task uploadTask = USER_REF
                        .child(uid)
                        .setValue(new User(email.getValue(), phone.getValue(), fullname.getValue(), imageUrl.getValue(), Role.RECEPTION,hotelId,adminHotelId));
                uploadTask.addOnSuccessListener(o -> addReceptionIsSuccessful.setValue(true));
            }
        });
    }
}
