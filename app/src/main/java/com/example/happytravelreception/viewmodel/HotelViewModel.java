package com.example.happytravelreception.viewmodel;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.Hotel;
import com.example.happytravelreception.model.User;
import com.example.happytravelreception.ultil.Role;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

public class HotelViewModel extends ViewModel {
    private static final DatabaseReference HOTEL_REF = FirebaseDatabase.getInstance().getReference("/hotels");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOTEL_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private List<Hotel> mList = new ArrayList<>();
    private final LiveData<List<Hotel>> hotelListLivedata = Transformations.map(liveData, new DeserializerList());
    public LiveData<List<Hotel>> getHotelListLivedata() {
        return hotelListLivedata;
    }
    private class DeserializerList implements Function<DataSnapshot, List<Hotel>> {
        @Override
        public List<Hotel> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                        Hotel hotel = snap.getValue(Hotel.class);
                        mList.add(hotel);
            }
            return mList;
        }
    }

    public MutableLiveData<String> imageUrl=new MutableLiveData<>();
    public MutableLiveData<String> hotelName=new MutableLiveData<>();
    public MutableLiveData<String> provine=new MutableLiveData<>();
    public MutableLiveData<String> district=new MutableLiveData<>();
    public MutableLiveData<String> standardStar=new MutableLiveData<>();

    public void getHotelDetail(String hotelId){
        HOTEL_REF.child(hotelId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Hotel hotel=new Hotel();
                hotel = snapshot.getValue(Hotel.class);
                imageUrl.setValue(hotel.getImageUrl());
                hotelName.setValue(hotel.getHotelName());
                provine.setValue(hotel.getProvine());
                district.setValue(hotel.getDistrict());
                standardStar.setValue(hotel.getStandardStar());
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }
    static String hotelname;
    public static String getHotelName(String hotelId){
        HOTEL_REF.child(hotelId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                hotelname=snapshot.child("hotelName").getValue(String.class);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return hotelname;
    }


    private final MutableLiveData<Boolean> hotelUpdateIsSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getHotelUpdateIsSuccessful() {
        return hotelUpdateIsSuccessful;
    }

    public void updateHotel(String hotelId) {
        Map<String, Object> updatemap = new HashMap<>();
        updatemap.put("imageUrl", imageUrl.getValue());
        updatemap.put("hotelName", hotelName.getValue());
        updatemap.put("provine", provine.getValue());
        updatemap.put("district", district.getValue());
        updatemap.put("standardStar", standardStar.getValue());
        Task uploadTask = HOTEL_REF
                .child(hotelId)
                .updateChildren(updatemap);
        uploadTask.addOnSuccessListener(o ->hotelUpdateIsSuccessful.setValue(true));
    }

    public String uploadImage(Intent intentData) {
        Uri selectedUri = intentData.getData();
        StorageReference photoRef = FirebaseStorage.getInstance()
                .getReference().child("hotels")
                .child(selectedUri.getLastPathSegment());

        UploadTask uploadTask = photoRef.putFile(selectedUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }
                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    imageUrl.setValue(String.valueOf(downloadUri));
                } else {
                }
            }
        });
        return imageUrl.getValue();
    }


}
