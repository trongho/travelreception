package com.example.happytravelreception.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;


import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class LoginViewModel extends ViewModel {
    private static final DatabaseReference USER_REF = FirebaseDatabase.getInstance().getReference("/user");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(USER_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private final MutableLiveData<FirebaseUser> userLiveData = new MutableLiveData<>();
    public MutableLiveData<FirebaseUser> getUserLiveData() {
        if (mAuth.getCurrentUser() != null) {
            userLiveData.postValue(mAuth.getCurrentUser());
        }
        return userLiveData;
    }

    private final MutableLiveData<Boolean> userLoginIsSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getUserLoginIsSuccessful() {
        return userLoginIsSuccessful;
    }

    public MutableLiveData<String> email=new MutableLiveData<>();
    public MutableLiveData<String> password=new MutableLiveData<>();

    public void login() {
        mAuth.signInWithEmailAndPassword(email.getValue(), password.getValue()).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    userLiveData.postValue(mAuth.getCurrentUser());
                    userLoginIsSuccessful.setValue(true);
                }
            }
        });
    }

    public void login(String email,String pw) {
        mAuth.signInWithEmailAndPassword(email, pw).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    userLiveData.postValue(mAuth.getCurrentUser());
                    userLoginIsSuccessful.setValue(true);
                }
            }
        });
    }
}
