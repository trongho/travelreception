package com.example.happytravelreception.viewmodel;

import android.content.Intent;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.User;
import com.example.happytravelreception.ultil.Common;
import com.example.happytravelreception.ultil.Role;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class UserViewmodel extends ViewModel {
    private static final DatabaseReference USER_REF = FirebaseDatabase.getInstance().getReference("/users");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(USER_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();
    FirebaseUser user = mAuth.getCurrentUser();

    private final MutableLiveData<FirebaseUser> userLiveData = new MutableLiveData<>();
    private final MutableLiveData<Boolean> userUpdateIsSuccessful = new MutableLiveData<>();
    private final MutableLiveData<Boolean> imageUpdateIsSuccessful = new MutableLiveData<>();

    public MutableLiveData<FirebaseUser> getUserLiveData() {
        if (mAuth.getCurrentUser() != null) {
            userLiveData.setValue(mAuth.getCurrentUser());
        }
        return userLiveData;
    }
    public MutableLiveData<Boolean> getUserUpdateIsSuccessful() {
        return userUpdateIsSuccessful;
    }

    public MutableLiveData<String> imageUrl=new MutableLiveData<>();
    public MutableLiveData<String> email=new MutableLiveData<>();
    public MutableLiveData<String> phone=new MutableLiveData<>();
    public MutableLiveData<String> fullname=new MutableLiveData<>();
    public MutableLiveData<String> userId=new MutableLiveData<>();

    public void getUserDetail(String uid){
            USER_REF.child(uid).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    User user = new User();
                    user = snapshot.getValue(User.class);
                    imageUrl.setValue(user.getAvatarUrl());
                    email.setValue(user.getEmail());
                    phone.setValue(user.getPhone());
                    fullname.setValue(user.getFullname());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {

                }
            });
    }

    public String getHotelId(String uid){
        USER_REF.child(uid).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                String hotelId=snapshot.child("hotelId").getValue(String.class);
                Common.HOTEL_ID=hotelId;
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
        return Common.HOTEL_ID;
    }

    public void updateUser() {
        String uid = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
        User user=new User(email.getValue(),phone.getValue(),fullname.getValue(),imageUrl.getValue(), Role.ADMIN_HOTEL);
        Task uploadTask = USER_REF
                .child(uid)
                .setValue(user);
        uploadTask.addOnSuccessListener(o ->userUpdateIsSuccessful.setValue(true));
    }

    public void logout() {
        mAuth.signOut();
    }

    public String uploadImage(Intent intentData){
        Uri selectedUri = intentData.getData();
        StorageReference photoRef = FirebaseStorage.getInstance()
                .getReference().child("hotels")
                .child(selectedUri.getLastPathSegment());

        UploadTask uploadTask = photoRef.putFile(selectedUri);
        Task<Uri> urlTask = uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                if (!task.isSuccessful()) {
                    throw task.getException();
                }

                // Continue with the task to get the download URL
                return photoRef.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Uri downloadUri = task.getResult();
                    imageUrl.setValue( String.valueOf(downloadUri));
                    imageUpdateIsSuccessful.setValue(true);
                } else {
                    imageUpdateIsSuccessful.setValue(false);
                }
            }
        });
        return imageUrl.getValue();
    }

    private List<User> mList = new ArrayList<>();
    private final LiveData<List<User>> userListLivedata = Transformations.map(liveData, new DeserializerList());
    public LiveData<List<User>> getUserListLivedata() {
        return userListLivedata;
    }
    private class DeserializerList implements Function<DataSnapshot, List<User>> {
        @Override
        public List<User> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                User user = snap.getValue(User.class);
                mList.add(user);
            }
            return mList;
        }
    }
}
