package com.example.happytravelreception.viewmodel;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.HotelBooking;
import com.example.happytravelreception.ultil.Status;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HotelBookingViewModel extends ViewModel {
    private static final DatabaseReference HOTEl_BOOKING_REF = FirebaseDatabase.getInstance().getReference("/hotelboookings");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(HOTEl_BOOKING_REF);
    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    private List<HotelBooking> mList = new ArrayList<>();
    private final LiveData<List<HotelBooking>> hotelbookingListLivedata = Transformations.map(liveData, new DeserializerList());
    public LiveData<List<HotelBooking>> getHotelbookingListLivedata() {
        return hotelbookingListLivedata;
    }
    private class DeserializerList implements Function<DataSnapshot, List<HotelBooking>> {
        @Override
        public List<HotelBooking> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                if(snap.child("status").getValue(String.class).equalsIgnoreCase("BOOKING")) {
                    HotelBooking hotelBooking = snap.getValue(HotelBooking.class);
                    mList.add(hotelBooking);
                }
            }
            return mList;
        }
    }

    private List<HotelBooking> mList2 = new ArrayList<>();
    private final LiveData<List<HotelBooking>> hotelbookingListLivedata2 = Transformations.map(liveData, new DeserializerList2());
    public LiveData<List<HotelBooking>> getHotelbookingListLivedata2() {
        return hotelbookingListLivedata2;
    }
    private class DeserializerList2 implements Function<DataSnapshot, List<HotelBooking>> {
        @Override
        public List<HotelBooking> apply(DataSnapshot input) {
            mList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                if(snap.child("status").getValue(String.class).equalsIgnoreCase("CHECKIN")) {
                    HotelBooking hotelBooking = snap.getValue(HotelBooking.class);
                    mList2.add(hotelBooking);
                }
            }
            return mList2;
        }
    }

    MutableLiveData<Boolean> bookingUpdateIsSuccessful=new MutableLiveData<>();
    public MutableLiveData<Boolean> getBookingUpdateIsSuccessful() {
        return bookingUpdateIsSuccessful;
    }
    public void updateHotelBooking(String bookingId) {
        Map<String, Object> updatemap = new HashMap<>();
        updatemap.put("status", Status.CHECKIN);
        Task uploadTask = HOTEl_BOOKING_REF
                .child(bookingId)
                .updateChildren(updatemap);
        uploadTask.addOnSuccessListener(o ->bookingUpdateIsSuccessful.setValue(true));
    }


    private final MutableLiveData<Boolean> removeBookingIsSuccessful = new MutableLiveData<>();
    public MutableLiveData<Boolean> getRemoveBookingIsSuccessful() {
        return removeBookingIsSuccessful;
    }
    public void removeBooking(String bookingId) {
        Task deleteTask = HOTEl_BOOKING_REF.child(bookingId).removeValue();
        deleteTask.addOnCompleteListener(o -> removeBookingIsSuccessful.setValue(true));
    }
}
