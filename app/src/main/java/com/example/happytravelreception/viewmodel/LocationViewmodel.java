package com.example.happytravelreception.viewmodel;

import androidx.arch.core.util.Function;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;

import com.example.happytravelreception.data.FirebaseQueryLiveData;
import com.example.happytravelreception.model.Location;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class LocationViewmodel extends ViewModel {
    private static final DatabaseReference LOCATION_REF =
            FirebaseDatabase.getInstance().getReference("/locations");
    private final FirebaseQueryLiveData liveData = new FirebaseQueryLiveData(LOCATION_REF);

    private List<String> provineList = new ArrayList<>();
    private List<String> districtList = new ArrayList<>();
    private List<Location> locationList = new ArrayList<>();

    public LiveData<List<String>> getProvineLiveData() {
        return provineLiveData;
    }

    public LiveData<List<String>> getDistrictLiveData() {
        return districtLiveData;
    }

    public LiveData<List<Location>> getLocationLiveData() {
        return locationLiveData;
    }

    private final LiveData<List<String>> provineLiveData =
            Transformations.map(liveData, new DeserializerListProvine());

    private final LiveData<List<String>> districtLiveData =
            Transformations.map(liveData, new DeserializerListDistrict());

    private final LiveData<List<Location>> locationLiveData =
            Transformations.map(liveData, new DeserializerLocation());

    private class DeserializerListProvine implements Function<DataSnapshot, List<String>> {
        @Override
        public List<String> apply(DataSnapshot input) {
            provineList.clear();
            provineList.add("Chọn thành phố");
            for (DataSnapshot snap : input.getChildren()) {
                String provineName = snap.child("name").getValue(String.class);
                provineList.add(provineName);
            }
            HashSet<String> hashSet = new HashSet<String>();
            hashSet.addAll(provineList);
            provineList.clear();
            provineList.addAll(hashSet);

            return provineList;
        }
    }

    private class DeserializerListDistrict implements Function<DataSnapshot, List<String>> {
        @Override
        public List<String> apply(DataSnapshot input) {
            districtList.clear();
            districtList.add("Chọn quận");
            for (DataSnapshot snap : input.getChildren()) {
                DataSnapshot districts=snap.child("districts");
                for (DataSnapshot snap2 : districts.getChildren()) {
                    String districtName=snap2.child("name").getValue(String.class);
                    districtList.add(districtName);
                }
            }
            return districtList;
        }
    }

    private class DeserializerLocation implements Function<DataSnapshot, List<Location>> {
        @Override
        public List<Location> apply(DataSnapshot input) {
            locationList.clear();
            for (DataSnapshot snap : input.getChildren()) {
                DataSnapshot districts=snap.child("districts");
                for (DataSnapshot snap2 : districts.getChildren()) {
                    Location location =new Location();
                    location.setProvine(snap.child("name").getValue(String.class));
                    location.setDistrict(snap2.child("name").getValue(String.class));
                    locationList.add(location);
                }
            }
            return locationList;
        }
    }

    public List<Location> districtByProvine(List<Location> list,String provineName){
        List<Location> newList=new ArrayList<>();
        Location location=new Location();
        for(int i=0;i<list.size();i++) {
            if (list.get(i).getProvine().equalsIgnoreCase(provineName)) {
                location=list.get(i);
                newList.add(location);
            }
        }
        return newList;
    }
}
