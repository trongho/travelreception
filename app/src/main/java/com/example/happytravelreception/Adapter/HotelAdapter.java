package com.example.happytravelreception.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ItemHotelBinding;
import com.example.happytravelreception.model.Hotel;

import java.util.List;

public class HotelAdapter extends RecyclerView.Adapter<HotelAdapter.ViewHolder> {
    private final static String TAG = "HotelAdapter";
    private List<? extends Hotel> mList ;
    private ItemClickListener mListenner;
    private ItemLongClickListener itemLongClickListener;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHotelBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_hotel,
                parent,false);
        return new ViewHolder(binding);
    }

    public HotelAdapter(List<? extends Hotel> mList, ItemClickListener mListenner, ItemLongClickListener itemLongClickListener) {
        this.mList=mList;
        this.mListenner=mListenner;
        this.itemLongClickListener=itemLongClickListener;
        notifyDataSetChanged();
    }

    public void setmListenner(ItemClickListener mListenner) {
        this.mListenner = mListenner;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Hotel hotel=mList.get(position);
        holder.binding.setHotel(mList.get(position));
        holder.binding.executePendingBindings();
        holder.binding.llitem.setOnClickListener(v->{
            if(mListenner!=null){
                mListenner.onClick(mList.get(position));
            }
        });
        holder.binding.llitem.setOnLongClickListener(v->{
            if(itemLongClickListener!=null){
                itemLongClickListener.onLongClick(mList.get(position));
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemHotelBinding binding;
        public ViewHolder(@NonNull ItemHotelBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        }

    public interface ItemClickListener {
        void onClick(Hotel hotel);
    }
    public interface ItemLongClickListener {
        boolean onLongClick(Hotel hotel);
    }


}
