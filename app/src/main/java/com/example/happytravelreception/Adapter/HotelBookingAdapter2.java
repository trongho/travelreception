package com.example.happytravelreception.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ItemHotelbooking2Binding;
import com.example.happytravelreception.databinding.ItemHotelbookingBinding;
import com.example.happytravelreception.model.HotelBooking;

import java.util.List;

public class HotelBookingAdapter2 extends RecyclerView.Adapter<HotelBookingAdapter2.ViewHolder> {
    private final static String TAG = "HotelBookingAdapter";
    private List<? extends HotelBooking> mList ;
    private ItemClickListener mListenner;
    private ItemLongClickListener itemLongClickListener;
    private CheckinClickListener checkinClickListener;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemHotelbooking2Binding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_hotelbooking2,
                parent,false);
        return new ViewHolder(binding);
    }

    public HotelBookingAdapter2(List<? extends HotelBooking> mList, ItemClickListener mListenner, ItemLongClickListener itemLongClickListener, CheckinClickListener checkinClickListener) {
        this.mList=mList;
        this.mListenner=mListenner;
        this.itemLongClickListener=itemLongClickListener;
        this.checkinClickListener=checkinClickListener;
        notifyDataSetChanged();
    }

    public void setmListenner(ItemClickListener mListenner) {
        this.mListenner = mListenner;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        HotelBooking hotelBooking=mList.get(position);
        holder.binding.setHotelbooking(mList.get(position));
        holder.binding.executePendingBindings();
        holder.binding.llitem.setOnClickListener(v->{
            if(mListenner!=null){
                    mListenner.onClick(mList.get(position));
            }
        });
        holder.binding.llitem.setOnLongClickListener(v->{
            if(itemLongClickListener!=null){
                itemLongClickListener.onLongClick(mList.get(position));
            }
            return true;
        });

        holder.binding.check.setOnClickListener(v->{
            if(checkinClickListener!=null){
                checkinClickListener.onClick(mList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemHotelbooking2Binding binding;
        public ViewHolder(@NonNull ItemHotelbooking2Binding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        }

    public interface ItemClickListener {
        void onClick(HotelBooking hotelBooking);
    }
    public interface ItemLongClickListener {
        boolean onLongClick(HotelBooking hotelBooking);
    }
    public interface CheckinClickListener {
        void onClick(HotelBooking hotelBooking);
    }


}
