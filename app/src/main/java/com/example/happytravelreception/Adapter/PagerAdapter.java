package com.example.happytravelreception.Adapter;

import android.content.Context;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;

import com.example.happytravelreception.R;
import com.example.happytravelreception.view.fragment.CheckinFragment;
import com.example.happytravelreception.view.fragment.WaitingCheckinFragment;

public class PagerAdapter extends FragmentStatePagerAdapter
{
    private static final int WAITING = 0;
    private static final int CHECKIN= 1;

    private static final int[] TABS = new int[]{WAITING,CHECKIN};

    private Context mContext;

    public PagerAdapter(final Context context, final FragmentManager fm)
    {
        super(fm);
        mContext = context.getApplicationContext();
    }

    @Override
    public Fragment getItem(int position)
    {
        switch (TABS[position])
        {
            case WAITING:
                return WaitingCheckinFragment.newInstance();
            case CHECKIN:
                return CheckinFragment.newInstance();
        }
        return null;
    }

    @Override
    public int getCount()
    {
        return TABS.length;
    }

    @Override
    public CharSequence getPageTitle(int position)
    {
        switch (TABS[position])
        {
            case WAITING:
                return mContext.getResources().getString(R.string.waiting);
            case CHECKIN:
                return mContext.getResources().getString(R.string.checkin);
        }
        return null;
    }
}
