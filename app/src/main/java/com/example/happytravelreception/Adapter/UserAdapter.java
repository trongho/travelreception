package com.example.happytravelreception.Adapter;

import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.happytravelreception.R;
import com.example.happytravelreception.databinding.ItemUserBinding;
import com.example.happytravelreception.model.User;

import java.util.List;

public class UserAdapter extends RecyclerView.Adapter<UserAdapter.ViewHolder> {
    private final static String TAG = "UserAdapter";
    private List<? extends User> mList ;
    private ItemClickListener mListenner;
    private ItemLongClickListener itemLongClickListener;
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemUserBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.item_user,
                parent,false);
        return new ViewHolder(binding);
    }

    public UserAdapter(List<? extends User> mList, ItemClickListener mListenner, ItemLongClickListener itemLongClickListener) {
        this.mList=mList;
        this.mListenner=mListenner;
        this.itemLongClickListener=itemLongClickListener;
        notifyDataSetChanged();
    }

    public void setmListenner(ItemClickListener mListenner) {
        this.mListenner = mListenner;
    }

    public void setItemLongClickListener(ItemLongClickListener itemLongClickListener) {
        this.itemLongClickListener = itemLongClickListener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        User user=mList.get(position);
        holder.binding.setUser(mList.get(position));
        holder.binding.executePendingBindings();
        holder.binding.llitem.setOnClickListener(v->{
            if(mListenner!=null){
                mListenner.onClick(mList.get(position));
            }
        });
        holder.binding.llitem.setOnLongClickListener(v->{
            if(itemLongClickListener!=null){
                itemLongClickListener.onLongClick(mList.get(position));
            }
            return true;
        });
    }

    @Override
    public int getItemCount() {
        return mList == null ? 0 : mList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ItemUserBinding binding;
        public ViewHolder(@NonNull ItemUserBinding binding) {
            super(binding.getRoot());
            this.binding=binding;
        }

        }

    public interface ItemClickListener {
        void onClick(User user);
    }
    public interface ItemLongClickListener {
        boolean onLongClick(User user);
    }


}
