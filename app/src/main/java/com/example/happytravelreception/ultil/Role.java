package com.example.happytravelreception.ultil;

public enum Role {
    USER,
    ADMIN,
    ADMIN_HOTEL,
    RECEPTION
}
