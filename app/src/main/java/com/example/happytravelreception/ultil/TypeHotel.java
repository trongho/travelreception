package com.example.happytravelreception.ultil;

public enum TypeHotel {
    RESORT,
    HOTEL,
    APARTMENT,
    HOMESTAY,
    VILLA,
    ECONOMY_HOTEL,
    CAPSULE_HOTEL,
    KARAOKE
}
