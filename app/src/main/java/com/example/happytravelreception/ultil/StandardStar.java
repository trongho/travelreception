package com.example.happytravelreception.ultil;

public enum StandardStar {
    ONE_STAR,
    TWO_STAR,
    THREE_STAR,
    FOUR_STAR,
    FIVE_STAR
}
