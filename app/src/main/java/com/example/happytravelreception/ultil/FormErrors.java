package com.example.happytravelreception.ultil;

public enum FormErrors {
    INVALID_FULLNAME,
    INVAID_EMAIL,
    INVALID_PHONE,
    INVALID_PASSWORD,
    PASSWORD_NOTMATCHING,
    MISSING_GENDER,
}
