package com.example.happytravelreception.ultil;

import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.databinding.BindingAdapter;
import androidx.databinding.InverseBindingAdapter;
import androidx.databinding.InverseBindingListener;
import androidx.databinding.InverseMethod;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.happytravelreception.Adapter.PagerAdapter;
import com.example.happytravelreception.R;
import com.example.happytravelreception.view.MainActivity;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.textfield.TextInputLayout;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Map;

public class BindingUtils {
    public static String attachCategories(Map<String,Boolean> categories){
        if(categories == null){
            return null;
        }

        //Filter categories whose values are true
        ArrayList<String> filteredList = new ArrayList<>();
        for (Map.Entry<String, Boolean> entry : categories.entrySet()) {
            if(entry.getValue()) {
                filteredList.add(entry.getKey());
            }
        }

        //Append categories in the list with comma as separator
        StringBuilder sb = new StringBuilder();
        Iterator<String> iterator = filteredList.iterator();
        while (iterator.hasNext()) {
            String element = iterator.next();
            sb.append(element);
            if (iterator.hasNext()) {
                sb.append(", ");
            }
        }

        return sb.toString();
    }

    @BindingAdapter({"imageUrl"})
    public static void setImageUrl(ImageView view, String imgurl) {
        Picasso.get().load(imgurl).placeholder(R.drawable.ic_image_black_24dp).into(view);
    }

    @BindingAdapter("app:errorText")
    public static void setErrorMessage(TextInputLayout view, String errorMessage) {
        view.setError(errorMessage);
    }

    @BindingAdapter("android:checked")
    public static void setChecked(CompoundButton view, boolean checked) {
        if (view.isChecked() != checked) {
            view.setChecked(checked);
        }
    }

    @BindingAdapter(value = {"android:onCheckedChanged", "android:checkedAttrChanged"},
            requireAll = false)
    public static void setListeners(CompoundButton view, final CompoundButton.OnCheckedChangeListener listener,
                                    final InverseBindingListener attrChange) {
        if (attrChange == null) {
            view.setOnCheckedChangeListener(listener);
        } else {
            view.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (listener != null) {
                        listener.onCheckedChanged(buttonView, isChecked);
                    }
                    attrChange.onChange();
                }
            });
        }
    }

    @BindingAdapter("android:text")
    public static void setText(TextView view, int value) {
        if (view.getText() != null
                && ( !view.getText().toString().isEmpty() )
                && Integer.parseInt(view.getText().toString()) != value) {
            view.setText(Integer.toString(value));
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static int getText(TextView view) {
        return Integer.parseInt(view.getText().toString());
    }

    @BindingAdapter("android:text")
    public static void setFloat(TextView view, float value) {
        if (view.getText() != null
                && ( !view.getText().toString().isEmpty() )
                && Float.parseFloat(view.getText().toString()) != value) {
            view.setText(Float.toString(value));
        }
    }

    @InverseBindingAdapter(attribute = "android:text")
    public static float getFloat(TextView view) {
        String num = view.getText().toString();
        if(num.isEmpty()) return 0.0F;
        try {
            return Float.parseFloat(num);
        } catch (NumberFormatException e) {
            return 0.0F;
        }
    }

    //date format
    @BindingAdapter("bindServerDate")
    public static void bindServerDate(@NonNull TextView textView, Object timestamp) {
        /*Parse string data and set it in another format for your textView*/
        SimpleDateFormat myFormat = new SimpleDateFormat("HH:mm:ss, EEE, dd MMMM yyyy");

        String timestampString= myFormat.format(new Date((long) timestamp));
        textView.setText(timestampString);
    }

    @BindingAdapter({"bind:handler"})
    public static void bindViewPagerAdapter(final ViewPager view, final Fragment fragment)
    {
        final PagerAdapter adapter = new PagerAdapter(view.getContext(),fragment.getFragmentManager()) ;
        view.setAdapter(adapter);
    }

    @BindingAdapter({"bind:pager"})
    public static void bindViewPagerTabs(final TabLayout view, final ViewPager pagerView)
    {
        view.setupWithViewPager(pagerView, true);
    }



}
