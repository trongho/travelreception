package com.example.happytravelreception.ultil;

public enum Status {
    PENDING,
    BOOKING,
    CHECKIN,
    CHECKOUT
}
