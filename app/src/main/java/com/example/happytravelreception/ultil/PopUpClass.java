package com.example.happytravelreception.ultil;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;

import com.example.happytravelreception.R;

import java.util.List;

public class PopUpClass {
    ListView listView;
    PopupItemSelected popupItemSelected;

    public PopUpClass(PopupItemSelected popupItemSelected) {
        this.popupItemSelected = popupItemSelected;
    }

    public void showPopupWindow(final View view, List<String> list, Context context) {
        LayoutInflater inflater = (LayoutInflater) view.getContext().getSystemService(view.getContext().LAYOUT_INFLATER_SERVICE);
        View popupView = inflater.inflate(R.layout.popup_window, null);

        //Specify the length and width through constants
        int width = LinearLayout.LayoutParams.WRAP_CONTENT;
        int height = LinearLayout.LayoutParams.WRAP_CONTENT;

        //Make Inactive Items Outside Of PopupWindow
        boolean focusable = true;

        //Create a window with our parameters
        final PopupWindow popupWindow = new PopupWindow(popupView, width, height, focusable);

        //Set the location of the window on the screen
        popupWindow.showAtLocation(view, Gravity.CENTER, 0, 0);


        //Initialize the elements of our window, install the handle
        listView = popupView.findViewById(R.id.text);
        ArrayAdapter<String> adapter=new ArrayAdapter<String>(  context,
                android.R.layout.simple_list_item_1,
                list);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                popupWindow.dismiss();
                //Toast.makeText(context,list.get(position),Toast.LENGTH_SHORT).show();
                if(popupItemSelected!=null) {
                    popupItemSelected.onClick(list.get(position));
                }
            }
        });


        //Handler for clicking on the inactive zone of the window

        popupView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                //Close the window when clicked
                popupWindow.dismiss();
                return true;
            }
        });
    }

    public interface PopupItemSelected{
        public void onClick(String result);
    }
}
